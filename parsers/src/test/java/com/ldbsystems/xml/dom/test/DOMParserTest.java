package com.ldbsystems.xml.dom.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ldbsystems.xml.dom.DOMParser;

public class DOMParserTest {

  DOMParser domParser;

  @Before
  public void setUp() throws Exception {

    // dom parsing
    OutputStreamWriter outputStreamWriter1 = new OutputStreamWriter(
        new FileOutputStream(
            new File(
                "C:\\Users\\velmuruganv\\Downloads\\revelmurugan\\output\\sample_xml_feed_enetpulse_soccer_output_dom.xml")));
    File file1 = new File(
        "C:\\Users\\velmuruganv\\Downloads\\revelmurugan\\sample_xml_feed_enetpulse_soccer.xml");
    FileInputStream fileInputStream1 = new FileInputStream(file1);
    domParser = new DOMParser(outputStreamWriter1, fileInputStream1);
    domParser.doDOMParse();

  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testParse() {
    domParser.doDOMParse();

  }

}
