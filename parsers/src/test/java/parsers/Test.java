package parsers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import com.ldbsystems.xml.dom.DOMParser;
import com.ldbsystems.xml.sax.SAX2Parser;

public class Test {
  public static void main(String[] args) {

    try {

      // sax parsing   

      OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
          new FileOutputStream(
              new File(
                  "C:\\Users\\velmuruganv\\Downloads\\revelmurugan\\output\\sample_xml_feed_enetpulse_tennis_output_sax.xml")));
      File file = new File(
          "C:\\Users\\velmuruganv\\Downloads\\revelmurugan\\sample_xml_feed_enetpulse_tennis.xml");
      FileInputStream fileInputStream = new FileInputStream(file);
      SAX2Parser handler = new SAX2Parser(outputStreamWriter, fileInputStream);
      handler.doSAXParse();

      // dom parsing
      OutputStreamWriter outputStreamWriter1 = new OutputStreamWriter(
          new FileOutputStream(
              new File(
                  "C:\\Users\\velmuruganv\\Downloads\\revelmurugan\\output\\sample_xml_feed_enetpulse_tennis_output_dom.xml")));
      File file1 = new File(
          "C:\\Users\\velmuruganv\\Downloads\\revelmurugan\\sample_xml_feed_enetpulse_tennis.xml");
      FileInputStream fileInputStream1 = new FileInputStream(file1);
      DOMParser domParser = new DOMParser(outputStreamWriter1, fileInputStream1);
      domParser.doDOMParse();
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
