package com.ldbsystems.xml.sax;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAX2Parser extends DefaultHandler {

  OutputStreamWriter osw = null;
  InputStream is = null;

  /**
   * @param osw
   * @param is
   *          TODO
   */
  public SAX2Parser(OutputStreamWriter osw, InputStream is) {
    super();
    this.osw = osw;
    this.is = is;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    int length = attributes.getLength();
    writeToFile(qName + ":");
    // System.out.print(qName + " : ");
    if (length > 1) {
      for (int i = 0; i < length; i++) {
        writeToFile(attributes.getLocalName(i) + " ");
        writeToFile(attributes.getValue(i) + " ");
        // System.out.print(attributes.getLocalName(i) + " - ");
        // System.out.print(attributes.getValue(i));
      }
      writeToFile("\n");
    }

  }

  private void writeToFile(String value) {
    try {
      osw.write(value);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void doSAXParse() {
    SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
    try {

      SAXParser saxParser = saxParserFactory.newSAXParser();
      SAX2Parser handler = new SAX2Parser(osw, is);

      saxParser.parse(is, handler);
      osw.flush();
      osw.close();
      is.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    // TODO
  }

  @Override
  public void characters(char ch[], int start, int length) throws SAXException {
    System.out.println("character is not coming ");
  }
}